import React, { useState, useEffect } from 'react';
import Dot from '../components/dot';
import useEventListener from '../hooks/useEventListener';

const { innerWidth: width, innerHeight: height } = window;
const defaultDistance = 30;
const maxDistance = 35;
const dotsPerLine = parseInt(width / defaultDistance) +1;
const dotLines = parseInt(height / defaultDistance);
let defaultDots = Array(dotLines).fill(Array(dotsPerLine).fill({ x: 100, y: 100 }));

const App = () => {
  const [distance, setDistance] = useState(defaultDistance);
  const [random, setRandom] = useState(false);

  useEventListener('click', () => setRandom(!random));
  useEffect(() => {
    const interval = setInterval(() => {
      setDistance(distance === defaultDistance ? maxDistance : defaultDistance);
    }, 2000)

    return () => clearInterval(interval);
  }, [distance]);

  return (
    <div>
      {
        defaultDots.map((line, lid) => {
          return line.map((d, did) => (
            <Dot
              random={random}
              width={width}
              height={height}
              lid={lid}
              did={did}
              distance={distance}
              size={5}
              center={{
                x: distance * parseInt(dotsPerLine / 2),
                y: distance * parseInt(dotLines / 2), 
              }}
              isCenter={lid === parseInt(dotLines / 2) && did === parseInt(dotsPerLine / 2)}
              key={`${did},${lid}`}
            />
            
          ))
        } 
        )

      }
    </div>
  );
}

export default App;
