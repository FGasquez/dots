import React, { useState, useEffect} from 'react';

const Item = ({ size, lid, did, height, width, distance, center = {}, random }) => {
  const [color, setColor] = useState('white');

  var originY = (center.y - (lid * distance - center.y));
  var originX = (center.x - (did * distance - center.x));
  if (random) {
    originX = Math.random() * width + 1;
    originY = Math.random() * height + 1;
  }


  if(lid === 5 && did === 5){
    console.log({
      did, 
      didC: distance * did, 
      lid, 
      lidC: distance * lid,
      distance, 
      center, 
      originX, 
      originY, 
    });
  }

  // const originX = distance * did;
  // const originY = distance * lid;
  const r = Math.sqrt(Math.pow(originX - center.x, 2) + Math.pow(originY - center.y, 2));
  // const angle = Math.atan2(originX - center.x, originY - center.y) * 180 / Math.PI;
  // positions as circle
  // const x = r * Math.cos(angle) + center.x;
  // const y = r * Math.sin(angle) + center.y;

  // const distanceToCenter = Math.sqrt((originY - center.y)**2 / (originX - center.x)**2);

  useEffect(() => {
    setColor('#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6));
  }, []);

  return (
    <div 
      className="item"
      style={{
        left: originX, 
        top:  originY, 
        backgroundColor: color, 
        padding: size+'px',
        transition: `500ms`,
        transitionDelay: `${r*2}ms`,
      }}
    />
  );
}

export default Item;
